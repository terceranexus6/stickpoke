#! /usr/bin/python

# The TCP ACK scan is not used to find the open or closed state of a port;
# It is used to find if a stateful firewall is present on the server or not. 
# It only tells if the port is filtered or not. This scan type cannot find the open/closed state of the port.

import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
from scapy.all import *
 
dst_ip = "10.0.0.1"
src_port = RandShort()
dst_port=80


ack_flag_scan_resp = sr1(IP(dst=dst_ip)/TCP(dport=dst_port,flags="A"),timeout=10)

# A TCP packet with the ACK flag set and the port number to connect to is sent to the server. 
# If the server responds with the RSP flag set inside a TCP packet, then the port is unfiltered and a stateful firewall is absent.
if (str(type(ack_flag_scan_resp))=="<type 'NoneType'>"):
    print "Stateful firewall present\n(Filtered)"

elif(ack_flag_scan_resp.haslayer(TCP)):
    if(ack_flag_scan_resp.getlayer(TCP).flags == 0x4):
        print "No firewall\n(Unfiltered)"
elif(ack_flag_scan_resp.haslayer(ICMP)):
    if(int(ack_flag_scan_resp.getlayer(ICMP).type)==3 and int(ack_flag_scan_resp.getlayer(ICMP).code) in [1,2,3,9,10,13]):
        print "Stateful firewall present\n(Filtered)"
