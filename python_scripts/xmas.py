#! /usr/bin/python
 
# In the XMAS scan, a TCP packet with the PSH, FIN, and URG flags set, along with
# the port to connect to, is sent to the server. 
# If the port is open, then there will be no response from the server.

import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
from scapy.all import *
 
dst_ip = "10.0.0.1"
src_port = RandShort()
dst_port=80
 
xmas_scan_resp = sr1(IP(dst=dst_ip)/TCP(dport=dst_port,flags="FPU"),timeout=10)
if (str(type(xmas_scan_resp))=="<type 'NoneType'>"):
    print "Open|Filtered"

# If the server responds with the RST flag set inside a TCP packet, 
# the port is closed on the server.
elif(xmas_scan_resp.haslayer(TCP)):
    if(xmas_scan_resp.getlayer(TCP).flags == 0x14):
        print "Closed"

# If the server responds with the ICMP packet with an ICMP unreachable error type 3 and ICMP code 1, 2, 3, 9, 10, or 13, 
# then the port is filtered and it cannot be inferred from the response whether the port is open or closed.
elif(xmas_scan_resp.haslayer(ICMP)):
    if(int(xmas_scan_resp.getlayer(ICMP).type)==3 and int(xmas_scan_resp.getlayer(ICMP).code) in [1,2,3,9,10,13]):
        print "Filtered"
