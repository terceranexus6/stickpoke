#! /usr/python

# In a NULL scan, no flag is set inside the TCP packet. The TCP packet is 
# sent along with the port number only to the server. 
 
import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
from scapy.all import *
 
dst_ip = "10.0.0.1"
src_port = RandShort()
dst_port=80
 
null_scan_resp = sr1(IP(dst=dst_ip)/TCP(dport=dst_port,flags=""),timeout=10)

# If the server sends no response to the NULL scan packet, then that particular port is open.
if (str(type(null_scan_resp))=="<type 'NoneType'>"):
    print("Open Filtered")

# If the server responds with the RST flag set in a TCP packet, then the port is closed on the server.
elif(null_scan_resp.haslayer(TCP)):
    if(null_scan_resp.getlayer(TCP).flags == 0x14):
        print("Closed")

# An ICMP error of type 3 and code 1, 2, 3, 9, 10, or 13 means the port is filtered on the server.
elif(null_scan_resp.haslayer(ICMP)):
    if(int(null_scan_resp.etlayer(ICMP).type)==3 and int(null_scan_resp.getlayer(ICMP).code) in [1,2,3,9,10,13]):
        print("Filtered")
