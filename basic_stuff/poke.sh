#!/bin/bash

RED='\033[0;31m'
PINK='\033[0;35'
BLUE='\033[0;34'
GREEN='\033[0;32'
NC='\033[0m' # No Color


function menu {
  #only printing
  echo -e "${BLUE}1 - Launch basic${NC}"
  echo -e "${PINK}2 - Second base ;)${NC}"
  echo -e "${RED}3 - help and info${NC}"
}

#$2 es la elección

function launch_basic {
  #create logs
  touch nkt.log
  touch nmap.log
  touch ports.log

  #getting basic info
  #$1 es la IP

  #nikto
  echo -e "${PINK}Starting enumeration...${NC}"

  echo -e "${BLUE}Getting info from nikto...${NC}"
  nikto -h $1 > nkt.log

  #nmap
  echo -e "${BLUE}Starting TCP scan....${NC}"
  echo -e "TCP\n" >> nmap.log
  nmap -sC -sV -vv -oA quick $1 >> nmap.log
  echo -e "\n" >> nmap.log

  echo -e "${BLUE}Starting UDP scan....${NC}"
  echo -e "UDP\n" >> nmap.log
  nmap -sU -sV -vv -oA quick_udp $1 >> nmap.log
  echo -e "\n" >> nmap.log

  #ports
  echo -e "${PINK}Getting ports information, catching 443... ${NC}"
  nc -v $1 443 >> ports.log
  echo -e "${PINK}Getting ports information, catching 80 (HTTP)... ${NC}"
  nc -v $1 80 >> ports.log
  echo -e "${PINK}Getting ports information, catching 23 (telnet)... ${NC}"
  nc -v $1 23 >> ports.log
  echo -e "${PINK}Getting ports information, catching 22 (SSH)... ${NC}"
  nc -v $1 22 >> ports.log
  echo -e "${PINK}Getting ports information, catching 25 (SMTP)... ${NC}"
  nc -v $1 25 >> ports.log
  echo -e "${PINK}Getting ports information, catching 53 (DNS)... ${NC}"
  nc -v $1 53 >> ports.log
  echo -e "${PINK}Getting ports information, catching 3389 (MTS)... ${NC}"
  nc -v $1 3389 >> ports.log
  echo -e "${PINK}Getting ports information, catching 119 (pop3)... ${NC}"
  nc -v $1 110 >> ports.log
  echo -e "${PINK}Getting ports information, catching 143 (IMAP)... ${NC}"
  nc -v $1 143 >> ports.log
  echo -e "${PINK}Getting ports information, catching 143 (IMAP)... ${NC}"
  nc -v $1 143 >> ports.log
  echo -e "${PINK}Getting ports information, catching 139 (NetBIOS-SSN)... ${NC}"
  nc -v $1 139 >> ports.log
  echo -e "${PINK}Getting ports information, catching 135 (MSRPC)... ${NC}"
  nc -v $1 135 >> ports.log
  echo -e "${PINK}Getting ports information, catching 3306 (MySQL)... ${NC}"
  nc -v $1 3306 >> ports.log
  #QUITAR DE LA VERSION ABIERTA
  echo -e "${PINK}Getting ports information, catching 1720 (Faith jump :) )... ${NC}"
  nc -v $1 1720 >> ports.log
  #QUITAR DE LA VERSION ABIERTA
  echo -e "${PINK}Getting ports information, catching 8080 (HTTP Proxy)... ${NC}"
  nc -v $1 8080 >> ports.log
  echo -e "${PINK}Getting ports information, catching 1723 (PPTP)... ${NC}"
  nc -v $1 1720 >> ports.log
  echo -e "${PINK}Getting ports information, catching 995 (POP3S)... ${NC}"
  nc -v $1 995 >> ports.log
  echo -e "${PINK}Getting ports information, catching 993 (IMAPS)... ${NC}"
  nc -v $1 993 >> ports.log
  echo -e "${PINK}Getting ports information, catching 5900 (VNC)... ${NC}"
  nc -v $1 5900 >> ports.log

  #port knock knock

  echo -e "Knock knock, who's there?"
  for x in 7000 8000 9000; do nmap -Pn –host_timeout 201 –max-retries 0 -p $x $1; done

  #nfs exposed
  showmount -e $1
  echo -e "${PINK}If no root found, execute ${NC}${GREEN}chown root:root sid-shell; chmod +s sid-shell${NC}"

  #getting info
  touch info.log
  whois $1 >> info.log
  host -a $1 >> info.log


  #python
  #python main.py

}

function more {
  #nmap
  echo -e "${PINK}Starting full nmap"
  nmap -sC -sV -p- -vv -oA full $1 >> nmap.log

  #nbt and rpc
  echo -e "${PINK}Starting rpcinfo...${NC}"
  rpcinfo -p $1 >> rpc.log
  echo -e "${PINK}Starting NBT scan...${NC}"
  nbtscan $1 >> nbtscan.log

  #kerberos
  nmap $TARGET -p 88 --script krb5-enum-users --script-args krb5-enum-users.realm='test' >> kerberosinfo.log
}

#menu()
mkdir logs
cd logs

if [ "$2" == "--basic" ]; then
  launch_basic
else
    echo "error"
fi
