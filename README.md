# Stick Poke

A collection of security scripts for enumeration, information gathering and more.

## Tools

**Languages**
- python
- bash

**tools**
- nmap
- showmount
- nikto
- nc
- whois



![](https://www.gnu.org/graphics/agplv3-with-text-162x68.png)